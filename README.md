# README #

Demo: http://thupham.bitbucket.org/ExplodedView/#/main
### Mô tả ###
Đây là angular directive phức tạp nhất mình đã phát triển cho dự án front-end gần nhất. 
Dự án yêu cầu chuyển đổi từ 1 ứng dụng flash sang ứng dụng Web sữ dụng lại phần back-end. Mục tiêu là ứng dụng phải chạy tốt được trên các trình duyệt PC (Chrome, IE, Firefox, ...) và di động (Safari,..)

### Yêu cầu của directive ###
* Người dùng có thể phóng to, thu nhỏ bằng chuột giữa hoặc bằng 1 button bên cạnh. Nếu bằng button thì điểm phóng to ở chính giữa, nếu bằng chuột thì điểm phóng to là vị trí chuột.
* Người dùng có thể drag-drop nhưng không được vượt quá khung hình ảnh.
* Người dùng có thể chọn từng thành phần của đồng hồ để xem chi tiết, giá cả,
* Khi đang trạng thái ban đầu, canvas sữ dụng hình ảnh chất lượng thấp, khi phóng to, để người dùng nhìn rõ hơn các thành phần, canvas sẽ chuyển thành hình ảnh chất lương cao,
* Các hoạt động của user trên directive cần được liên kết với controller như zoom, chọn
* Hoạt động mượt mà trên các trình duyệt