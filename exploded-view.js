angular.module('sampleApp').directive('explodedView', function() {
    return {
        restrict: 'A',
        scope:{
            canvasData: '=',
            piecesData: '=',
            imageScale: "=",
            reachFunction: "&",
            highlightFunction: "&",
            setScale: "&"
        },
        link: function(scope, element) {
            // ====================== variables declaration ===============
            var maskCanvas = element[0];
            var maskCtx = maskCanvas.getContext('2d');

            var highlightCanvas = document.getElementById('highlightCanvas');
            var highlightCtx = highlightCanvas.getContext('2d');

            var dataCanvas = document.getElementById('dataCanvas');
            var dataCtx = dataCanvas.getContext('2d');

            var highlightCanvasOriginal = document.getElementById('highlightCanvasOriginal');
            var highlightCtxOriginal = highlightCanvasOriginal.getContext('2d');

            var dataCanvasOriginal = document.getElementById('dataCanvasOriginal');
            var dataCtxOriginal = dataCanvasOriginal.getContext('2d');

            var maskLQImage, dataImage, maskHQImage;
            var canvasWidth, canvasHeight;

            var startMove = 0;
            var lastX, lastY;
            var dragStart;

            var maxScaler = 0;

            var sizeDrawW = 0;
            var sizeDrawH = 0;

            var isHQImageLoaded = false;
            var isDataImageLoaded = false;

            //============== extend function ========================

            //Extend image object to use SVG Coordinate Systems and some utilities function
            function extendImage(image){
                var svg = document.createElementNS("http://www.w3.org/2000/svg",'svg');
                var xform = svg.createSVGMatrix();

                image.getScale = function(){
                    return xform.a;
                };

                image.getTransform = function(){
                    return xform;
                };

                image.scale = function(sx,sy){
                    xform = xform.scaleNonUniform(sx,sy);
                };

                image.rotate = function(radians){
                    xform = xform.rotate(radians*180/Math.PI);
                };

                var downPoint = {};
                image.mouseDown = function(){
                    downPoint = {x:xform.e, y:xform.f};
                };

                image.mouseEnd = function(){
                    downPoint = {};
                };

                image.translateImage = function(dx,dy){
                    image.update(downPoint.x + dx, downPoint.y + dy, xform.a, 0, 1);
                };

                image.transform = function(a,b,c,d,e,f){
                    var m2 = svg.createSVGMatrix();
                    m2.a=a; m2.b=b; m2.c=c; m2.d=d; m2.e=e; m2.f=f;
                    xform = xform.multiply(m2);
                };

                image.setTransform = function(a,b,c,d,e,f){
                    xform.a = a;
                    xform.b = b;
                    xform.c = c;
                    xform.d = d;
                    xform.e = e;
                    xform.f = f;
                };

                image.copyTransform = function(context){
                    var contextXForm = context.getTransform();
                    xform.a = contextXForm.a;
                    xform.b = contextXForm.b;
                    xform.c = contextXForm.c;
                    xform.d = contextXForm.d;
                    xform.e = contextXForm.e;
                    xform.f = contextXForm.f;
                };

                image.zoomTo = function(scale, point){
                    TweenMax.to(xform, 0.5 ,{e: (canvasWidth-sizeDrawW*scale)/2, f:(canvasHeight-sizeDrawH*scale)/2, a:scale, d:scale, ease:Cubic.easeOut});
                };

                image.zoomIn = function(point){
                    scope.setScale()(2);
                    image.setFocus(point.x, point.y);
                    var scale = xform.a + (maxScaler-1)/5;
                    var targetOffsetX = focusX-focusX1*scale;
                    var targetOffsetY = focusY-focusY1*scale;
                    image.update(targetOffsetX, targetOffsetY, scale, 0.2);

                };

                image.zoomOut = function(point){

                    image.setFocus(point.x, point.y);
                    var scale = xform.a - (maxScaler-1)/5;
                    var targetOffsetX = focusX-focusX1*scale;
                    var targetOffsetY = focusY-focusY1*scale;
                    image.update(targetOffsetX, targetOffsetY, scale, 0.2);
                };
                var focusX, focusY, focusX1, focusY1;
                image.setFocus = function(mx, my){
                    focusX = mx;
                    focusY = my;
                    focusX1 = parseInt((mx-xform.e)/xform.a);
                    focusY1 = parseInt((my-xform.f)/xform.d);
                };

                image.zoomContent = function(){

                };

                image.validateBound = function(x, y, s){
                    if(s < 1){
                        s = 1;
                    }
                    if(s > maxScaler){
                        s = maxScaler;
                    }
                    var minX = canvasWidth - s*sizeDrawW;
                    var maxX = 0;
                    var minY = canvasHeight - s*sizeDrawH;
                    var maxY = 0;

                    if(minX > maxX){
                        x = (minX-maxX)/2;
                    } else {
                        if(x > maxX){
                            x = maxX;
                        }
                        if(x < minX){
                            x = minX;
                        }
                    }
                    if(minY > maxY){
                        y = (minY-maxY)/2;
                    } else {
                        if(y > maxY){
                            y = maxY;
                        }
                        if(y < minY){
                            y = minY;
                        }
                    }
                    return {x:x, y:y, s:s};
                };

                image.update = function(xx, yy, scale, time, isTranslate){

                    if(scale.toFixed(2) <= maxScaler.toFixed(2)){
                        var target = image.validateBound(xx, yy, scale);
                        TweenMax.killTweensOf(xform);
                        if(isTranslate){
                            TweenMax.to(xform, time ,{e: target.x, f:target.y, ease:Cubic.easeOut});
                        } else{
                            TweenMax.to(xform, time ,{e: target.x, f:target.y, a:target.s, d:target.s, ease:Cubic.easeOut});
                        }

                    }
                    else {
                        scale = maxScaler;
                    }
                    if(angular.isUndefined(isTranslate)){
                        if(!maxScaler){
                            scope.setScale()(1);
                        }
                        if(scale.toFixed(2) > 1){
                            scope.setScale()(2);
                        } else {
                            scope.setScale()(1);
                        }
                    }

                };

                image.getRectangle = function(){
                    return {
                        w: sizeDrawW * xform.a,
                        h: sizeDrawH * xform.d,
                        x: xform.e,
                        y: xform.f
                    };
                };
            }

            //Extend 2d context object to use some utilities function
            function extendDataContext(context){
                var rgbaToDecimalColor = function(r, g, b, a) {
                    if (r > 255 || g > 255 || b > 255 || a > 255) {
                        throw "Invalid color component";
                    }
                    return (a << 24) + (r << 16) + (g << 8) + b;
                };
                context.getPointColor = function(x, y){
                    var imageData = context.getImageData(x, y, 1, 1);
                    return rgbaToDecimalColor(imageData.data[0],imageData.data[1],imageData.data[2],imageData.data[3]);
                };

                context.processImageData = function(colorArray){
                    var dataToProcess = context.getImageData(0, 0, canvasWidth, canvasHeight).data;
                    var imageDataToChange = dataCtxOriginal.getImageData(0, 0, canvasWidth, canvasHeight);
                    var dataToChange = imageDataToChange.data;
                    for(var i=0; i<dataToProcess.length; i+=4){
                        var colorImage = rgbaToDecimalColor(dataToProcess[i], dataToProcess[i+1], dataToProcess[i+2], dataToProcess[i+3]);
                        if(colorArray.indexOf(colorImage)>-1){
                            dataToChange[i] = 255;
                            dataToChange[i + 1] = 128;
                            dataToChange[i + 2] = 0;
                            dataToChange[i + 3] = 128;
                        } else {
                            dataToChange[i] = 0;
                            dataToChange[i + 1] = 0;
                            dataToChange[i + 2] = 0;
                            dataToChange[i + 3] = 0;
                        }
                    }
                    imageDataToChange.data = dataToChange;
                    return imageDataToChange;
                };
            }

            //======================function========================
            //Main update of the canvas
            function update(){
                maskCtx.clearRect(0, 0, canvasWidth, canvasHeight);
                var rect = maskLQImage.getRectangle();
                if(maskLQImage.getScale() > 1 && isHQImageLoaded === true){
                    maskCtx.drawImage(maskHQImage, rect.x, rect.y, rect.w, rect.h);
                } else {
                    maskCtx.drawImage(maskLQImage, rect.x, rect.y, rect.w, rect.h);
                }

                if(isDataImageLoaded === true){
                    dataCtx.clearRect(0, 0, canvasWidth, canvasHeight);
                    dataCtx.drawImage(dataImage, rect.x, rect.y, rect.w, rect.h);
                }

                highlightCtx.clearRect(0, 0, canvasWidth, canvasHeight);
                highlightCtx.drawImage(highlightCanvasOriginal, rect.x, rect.y, rect.w, rect.h);
            }

            // Init the size of canvas to fit with multiple device
            function initSizeOfCanvas(){
                canvasWidth = maskCanvas.offsetWidth;
                canvasHeight = maskCanvas.offsetHeight;

                maskCanvas.width = canvasWidth;
                maskCanvas.height = canvasHeight;
                dataCanvas.width = canvasWidth;
                dataCanvas.height = canvasHeight;
                highlightCanvas.width = canvasWidth;
                highlightCanvas.height = canvasHeight;
                highlightCanvasOriginal.width = canvasWidth;
                highlightCanvasOriginal.height = canvasHeight;
                dataCanvasOriginal.width = canvasWidth;
                dataCanvasOriginal.height  = canvasHeight;
            }

            //Init and draw the canvas
            function initCanvas(){
                initSizeOfCanvas();

                maskLQImage = new Image();
                maskLQImage.src = "data:image/png;base64," + scope.canvasData.exploded.picture;
                extendImage(maskLQImage);
                maskLQImage.onload = function(){
                    if(canvasWidth < maskLQImage.width || canvasHeight < maskLQImage.height){
                        if(canvasWidth/canvasHeight < maskLQImage.width/maskLQImage.height){
                            sizeDrawW = canvasWidth;
                            sizeDrawH = sizeDrawW/(maskLQImage.width/maskLQImage.height);
                        } else {
                            sizeDrawH = canvasHeight;
                            sizeDrawW = sizeDrawH*(maskLQImage.width/maskLQImage.height);
                        }
                    } else {
                        sizeDrawW = maskLQImage.width;
                        sizeDrawH = maskLQImage.height;
                    }
                    maskLQImage.setTransform(1, 0, 0, 1, (canvasWidth-sizeDrawW)/2, (canvasHeight-sizeDrawH)/2);
                    TweenLite.ticker.addEventListener("tick", update);
                };

                extendDataContext(dataCtx);
                extendDataContext(dataCtxOriginal);
                dataImage = new Image();
                dataImage.src = "data:image/png;base64," + scope.canvasData.exploded.mask;
                dataImage.onload = function(){
                    isDataImageLoaded = true;
                    dataCtxOriginal.drawImage(dataImage, 0, 0, canvasWidth, canvasHeight);
                };
            }

            // ====================== event watchers ===============
            // Event listener when canvasData request come
            scope.$watch('canvasData', function(newValue, oldValue){
                if(newValue !== oldValue){
                    initCanvas();
                }
            });

            // Event listener when High Quality picture come request come
            scope.$watch('canvasData.exploded.pictureHQ', function(newValue, oldValue){
                if(newValue !== oldValue){
                    maskHQImage = new Image();
                    maskHQImage.src = "data:image/png;base64," + scope.canvasData.exploded.pictureHQ;
                    maskHQImage.onload = function(){
                        isHQImageLoaded = true;
                        maxScaler = maskHQImage.width/sizeDrawW;
                    };
                }
            });

            // Event listener when user ask to zoom canvas
            scope.$on("zoomExplodedView", function(event, args){
                if(args.isZoomIn === true){
                    if(maxScaler){
                        scope.setScale()(2);
                        maskLQImage.zoomTo(maxScaler,{x:canvasWidth/2, y:canvasHeight/2});
                    }
                } else {
                    if(maskLQImage.zoomTo){
                        scope.setScale()(1);
                        maskLQImage.zoomTo(1,{x:canvasWidth/2, y:canvasHeight/2});
                    }
                }
            });

            var isContain = function(list1, list2){
                for(var i=0; i<list1.length; i++){
                    for(var j=0; j<list2.length; j++){
                        if(list1[i].rmccode === list2[j].rmccode){
                            return true;
                        }
                    }
                }
            };

            // Event listener when user ask to highlight a part
            scope.$on("highLightRMC", function(event, args) {
                var highLightColor = [];
                if (scope.piecesData) {
                    for (var i = 0; i < scope.piecesData.length; i++) {
                        if (isContain(scope.piecesData[i].rmclist, args.list)) {
                            highLightColor.push(scope.piecesData[i].refColor);
                        }
                    }

                    var imageAfterProcess = dataCtxOriginal.processImageData(highLightColor);

                    highlightCtxOriginal.putImageData(imageAfterProcess, 0, 0);
                }
            });

            // ====================== initial state setup ===============

            // Add event listener for the top canvas
            var handleScroll = function(event){
                event.preventDefault();
                var delta = (event.originalEvent && event.originalEvent.detail < 0) || event.wheelDelta > 0 ? 1 : -1;
                if(delta > 0){
                    maskLQImage.zoomIn({x:lastX, y:lastY});
                } else {
                    maskLQImage.zoomOut({x:lastX, y:lastY});
                }
            };

            var handleStart = function(event){
                event.preventDefault();
                startMove = 0;
                lastX = event.offsetX || (event.pageX - highlightCanvas.offsetLeft);
                lastY = event.offsetY || (event.pageY - highlightCanvas.offsetTop);

                dragStart = {x:lastX,y:lastY};
                maskLQImage.mouseDown();
            };

            var handleEnd = function(event){
                event.preventDefault();
                dragStart = null;
                maskLQImage.mouseEnd();

                var value = dataCtx.getPointColor(lastX, lastY);
                if(value !== 0 && startMove < 10){
                    if(scope.piecesData){
                        for(var i=0; i<scope.piecesData.length; i++){
                            if(scope.piecesData[i].refColor === value){
                                if(scope.piecesData[i].link) {
                                    scope.reachFunction()(scope.piecesData[i]);
                                } else {
                                    scope.highlightFunction()(scope.piecesData[i]);
                                }
                            }
                        }
                    }
                }
            };

            var handleMove = function(event){
                event.preventDefault();
                event.target.style.cursor = 'default';

                startMove += 1;

                lastX = event.offsetX || (event.pageX - highlightCanvas.offsetLeft);
                lastY = event.offsetY || (event.pageY - highlightCanvas.offsetTop);


                var value = dataCtx.getPointColor(lastX, lastY);

                if(value !== 0){
                    event.target.style.cursor = "pointer";
                }

                if (dragStart){
                    maskLQImage.translateImage(lastX - dragStart.x, lastY - dragStart.y);
                }
            };

            var handleOut = function(){
                dragStart = null;
            };

            var getTouchPos = function(canvas, touchEvent){
                var touch = touchEvent.changedTouches[0];
                return {
                    x: touch.pageX,
                    y: touch.pageY
                };
            };

            var dispatchMouseEvent = function(event, name){
                var mousePos = getTouchPos(highlightCanvas, event);

                var mouseEvent = new MouseEvent(name, {
                    clientX: mousePos.x,
                    clientY: mousePos.y
                });

                highlightCanvas.dispatchEvent(mouseEvent);
            };

            // User need to wait before we init-ed everything
            setTimeout(function(){
                highlightCanvas.addEventListener("mousemove", handleMove, false);
                highlightCanvas.addEventListener('mouseup',handleEnd,false);
                highlightCanvas.addEventListener("mousedown", handleStart, false);
                highlightCanvas.addEventListener("touchstart", function(e){
                    dispatchMouseEvent(e, "mousedown");
                }, false);
                highlightCanvas.addEventListener("touchend", function(e){
                    dispatchMouseEvent(e, "mouseup");
                }, false);
                highlightCanvas.addEventListener("touchmove", function(e){
                    dispatchMouseEvent(e, "mousemove");
                }, false);
                highlightCanvas.addEventListener("touchcancel", function(e){
                    dispatchMouseEvent(e, "mouseup");
                }, false);
                highlightCanvas.addEventListener("mouseout", handleOut, false);
                highlightCanvas.addEventListener('DOMMouseScroll',handleScroll,false);
                highlightCanvas.addEventListener('mousewheel',handleScroll,false);
            }, 3000);

        }
    };
});
